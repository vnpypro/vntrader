#pragma once
// ---- 计算k线的类 ---- //

#include <vector>
#include <string>
#include "stdafx.h"

class CTickToKline
{
public:
	// 从本地数据构建k线，并存储到本地(假定本地数据没有丢包)
	void KLineFromLocalData(const std::string &sFilePath, const std::string &dFilePath); 
	// 从实时数据构建k线
	bool KLineM1FromTick(CThostFtdcDepthMarketDataField *pDepthMarketData);

public:
	  std::vector<double> m_priceVec; // 存储1分钟的价格
	  std::vector<int> m_volumeVec; // 存储1分钟的成交量

	  KLineDataType m_KLinelastM1;
	  std::vector<KLineDataType> m_KLineM1;
	  int lastmin = -1;
};
